const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routes for userRoutes.js
const userRoutes = require("./routes/userRoutes");

// routes for productRoutes.js
const productRoutes = require("./routes/productRoutes");

// routes for orderRoutes.js
const orderRoutes = require("./routes/orderRoutes");

// routes for cartRoutes.js
const cartRoutes = require("./routes/cartRoutes");

const app = express();

// connected to MongoDB Atlas
mongoose.connect("mongodb+srv://admin123:admin123@project0.4hc3b3v.mongodb.net/capstone-2-gordula?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.once('open', () => console.log("It is now connected to MongoDB Atlas."));


// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now running on port ${process.env.PORT || 4000}`);
});
const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartControllers');
const auth = require('../auth');


/*****************************  Add-To-Cart ******************************/
router.post('/', auth.verify, (req, res) => {
  
  let data = {
    cart: req.body,
    cartId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  }


  cartController.addToCart(data, req.body).then((resultFromController) => res.send(resultFromController));
});


/**************************  Retrieving All Cart ***********************/
router.get('/', auth.verify, (req, res) => {
  const data = {
    cart: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  
  cartController.getAllCart(data).then((resultFromController) => res.send(resultFromController));
});

/**************************  Retrieving All Cart ***********************/
router.get('/:id', auth.verify, (req, res) => {
  const data = {
    order: req.params,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  
  cartController.getSpecificCart(data, req.params).then((resultFromController) => res.send(resultFromController));
});


module.exports = router;
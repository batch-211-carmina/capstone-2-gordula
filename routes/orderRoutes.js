const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth');


/*************************** Route for Add Order ************************/
/*router.post('/', auth.verify, (req, res) => {
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  console.log('\n===========\n', data);

  if (data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Non-Admin only]"});
        return;
    }

  orderController.addOrder(data).then((resultFromController) => res.send(resultFromController));
});*/

router.post("/", auth.verify, (req,res)=>{
  
  let data = {
    order: req.body,
    orderId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  }

  orderController.addOrder(data).then(resultFromController => res.send(resultFromController));
});


/************************* Route for Retrieving All Order **********************/
router.get('/', auth.verify, (req, res) => {
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  if (!data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Admin-only]"});
        return;
    }
  
  orderController.getOrders(data).then((resultFromController) => res.send(resultFromController));
});


/************** Route for Retrieving Authenticated User's Order ***************/
router.get('/getUserOrders', auth.verify, (req, res) => {
  const data = {
    userId: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  if (data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation."});
        return;
    }
  
  orderController.getUserOrders(data, req.body).then((resultFromController) => res.send(resultFromController));
});


/************************** Route for Cancelling Orders ***********************/
router.put('/:orderId', auth.verify, (req, res) => {
  
  orderController.cancelOrder(req.params).then((resultFromController) => res.send(resultFromController));
});




module.exports = router;
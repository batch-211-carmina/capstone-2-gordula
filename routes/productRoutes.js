const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


/************************* Route for Creating a Product **********************/
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (!data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Admin-only]"});
        return;
    }

	productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});


/********************** Route for Retrieving All Products *********************/
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send (resultFromController));
});


/******************* Route for Retrieving All Active Products *****************/
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send (resultFromController));
});


/******************* Route for Retrieving a Single Products ******************/
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


/******************** Route for Update Product Information *******************/
router.put("/:productId", auth.verify, (req, res) => {

    const data = {
        id: req.params.id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    if (!data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Admin-only]"});
        return;
    }

    productController.updateProduct(req.params, data, req.body).then(resultFromController => res.send(resultFromController));
});


/************************ Route for Archive a Product ***********************/
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
	    id: req.params.id,
	    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  	};

  	if (!data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Admin-only]"});
        return;
    }

	productController.archiveProduct(req.params, data).then((resultFromController) => res.send(resultFromController));

});


/*********************** Route for Activate a Product **********************/
router.put("/:productId/activate", auth.verify, (req, res) => {

	const data = {
		id: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (!data.isAdmin) {
        res.send({message: "You don't have the rights to perform this operation [Admin-only]"});
        return;
    }

	productController.activateProduct(req.params, data).then((resultFromController) => res.send(resultFromController));
});



module.exports = router;
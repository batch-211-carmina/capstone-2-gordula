const express = require("express");
const router = express.Router();

// to access the controllers folder
const userController = require("../controllers/userControllers");
const auth = require("../auth");


/************************************************************************/
// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

/************************************************************************/
// Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

/************************************************************************/
// Admin User Registration
router.post("/registerAdmin", (req, res) => {
	userController.registerAdminUser(req.body).then(resultFromController => res.send(resultFromController));
});

/************************************************************************/
// User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


/************************************************************************/
// Retrieve User Details
router.get("/getUserdetails", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getUserdetails({userId:userData.id}).then(resultFromController => res.send(resultFromController));
});


/************************************************************************/
// Set User as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    const data = {
        id: req.params.id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    if (!data.isAdmin) {
        res.send({message: "You don't have rights to perform this operation [Admin-only]"});
        return;
    }

    userController.setUserAdmin(req.params, data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
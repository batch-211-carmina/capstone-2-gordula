const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

      userId: {
        type: String,
        required: true
      },
      totalAmount: {
        type: Number,
        required: true
      },
      purchaseOn: {
         type: Date,
         default: new Date()
      },
      orderStatus: {
      //rejected: 0, cancelled: 1, toPay : 2, toShip: 3, toReceive: 4, toRate: 5

        type: Number,
        default: 2,
      },
      products: [
        {
              productId: {
                type: String,
                required: true
              },
              quantity: {
              type: Number,
              required: true
            }
        }
      ]
      
});

module.exports = mongoose.model('Order', orderSchema);
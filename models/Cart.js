const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

	productId: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	quantity: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		required: true
	}
});

module.exports = mongoose.model("Cart", cartSchema);
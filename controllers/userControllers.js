const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/********************* Checking email if already exists **********************/
/*
	STEPS
	1. Use mongoose "find" method to find duplicates emails
	2. Use the "then" method to send a response back to the application based on the result of the "find" method
*/
module.exports.checkEmailExist = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		}
		else {
			return false;
		}
	});
};

/*************************** User Registration ******************************/
/*
	STEPS:
	1. Create a new User object using the mongoose models and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	});

			return User.find({email:reqBody.email}).then(result => {
				if(result.length > 0) {
					return {message: "Email Already Used!"}
				}
				else {
					return newUser.save().then((user, error) => {
				if (error) {
					return false;
				}
				else {
					return { 
						message: `${user.firstName} ${user.lastName} is successfully registered!`,
						data: user
					};
				}
			});
		}
	});
	
};



/************************** Admin User Registration ************************/
/*
	STEPS:
	1. Create a new User object for admin using the mongoose models and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerAdminUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	});

		return User.find({email:reqBody.email}).then(result => {
			if(result.length > 0) {
				return {message: "Email Already Used!"}
			}
			else {
				return newUser.save().then((user, error) => {
					if (error) {
						return false;
					}

					user.isAdmin = true;
					return newUser.save().then((user, err) => {
						if(err) {
							return false;
						}
						else {
							return {
								message: `${user.firstName} ${user.lastName} is successfully registered as an Admin!`,
								data: user
							};
						}
					});
				});
			}
		});

};

/************************** User Authentication ***************************/
/*
	STEPS:
	1. Check the database if the user email already exists
	2. Compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web Token if the user is successfully login and return message if not
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null && result != reqBody.email) {
			return {message: "No email found!"};
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {
					message: "You are now logged in!",
					access: auth.createAccessToken(result)
				};
			} else {
				return false;
			}
		}
	});
};


/************************ Retrieve User Details *************************/
/*
	STEPS
	1. Find the document in the database using the user's ID (with authentication)
	2. Reassign password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getUserdetails = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};


/************************ Set User as Admin *************************/
module.exports.setUserAdmin = (reqParams, data) => {
   
   let setUserAsAdmin = {
   		isAdmin : true
   };
   if (data.isAdmin) {
	   return User.findByIdAndUpdate(reqParams.userId, setUserAsAdmin).then((result, error) => {
	   		if (error) {
	   			return false;
	   		}
	   		else {
	   			console.log(result);
	   			return {
	   				message: "This ID is now an Admin User!",
	   				data: setUserAsAdmin
	   			};
	   		}
	   });
	}
	
};

const Order = require("../models/Order");
const Product = require("../models/Product");


/*************************** Add a New Order *****************************/
module.exports.addOrder = async (data) => {
  let order = new Order(data.order);
  
  order = await order.save().then(order => {
    if (order) {
      return order;
    }
    else {
      return false;
    }
  });

  for (let i = 0; i < data.order.products.length; i++) {
    await Product.findById(data.order.products[i].productId).then(product => {

    product.orders.push({orderId:order.id, quantity:data.order.products[i].quantity});

    product.save().then((product,error) => {

      if (error) {
        return false;
      }
      else {
        return true;
      };
    });
  });
  }

  if (order) {
    return order;
  }
  else {
    return false;
  }
};


/**************************  Retrieving All Orders ***********************/
module.exports.getOrders = (data) => {

  if (data.isAdmin)
  return Order.find().then(
    (result, error) => {
      if (error) {
        return false;
      }
        return result;
      }
    ); 
};


/******************  Retrieving Authenticated User's Order ***************/
/*module.exports.getUserOrders = (data, reqBody) => {

  if (!data.isAdmin)
  return Order.find({userId:reqBody.userId}).then((result, error) => {
      if (error) {
        return false;
      }
        return result;
      }
    ); 

};*/

module.exports.getUserOrders = (data, reqBody) => {

  if (!data.isAdmin)
  return Order.findOne(reqBody.userId)
    .then(result => {
      return result;
    })
    .catch(err => {
      return {message: "Unsuccessful!"}
    })
};


/****************************  Cancel an Order *************************/
module.exports.cancelOrder = (reqParams) => {

    let orderCancelStatus = {
      orderStatus : 1
   };

  return Order.findByIdAndUpdate(reqParams.orderId, orderCancelStatus).then(
    (result, error) => {
      if (error) {
        return false;
      }
        console.log(result);
        return {
          message: "Your order has been cancelled!",
          data: orderCancelStatus
        }
      }
    ); 
};

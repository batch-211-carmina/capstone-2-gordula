const Product = require("../models/Product");

/************************* Create a New Product ***************************/
/*
	STEP:
	1. Create a new Product object using the mongoose model and the information from the reqBody and (the id from the header)
	2. Save the new User to the database
*/
module.exports.addProduct = (data, reqBody) => {
    let newProduct = new Product (data.product);

    return Product.find({name:reqBody.name}).then(result => {
		if(result.length > 0) {
			return {message: "Product name already exist!"}
		}
		else {
			if (data.isAdmin) {
				return newProduct.save().then((result, error) => {
					return {
						message: "New Product Added!",
						data: result
					};
				});
			}
			else {
				return false;
			}
		}
	});
  
};


/************************* Retrieve All Products **************************/
/*
	STEP:
	1. Retrieve all the product from the database using the find() method
*/
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


/********************* Retrieve All Active Products ***********************/
/*
	STEPS:
	1. Find all active products in the database
	2. Retrun the result back to the frontend
*/
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then((result, error) => {
		if (error) {
			return false;
		}
		else {
			return result;
		}
	});
};


/********************** Retrieve a Single Products ***********************/
/*
	STEP:
	1. Get the product by id using the Mongoose method "findById"
*/
/*module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};*/

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId)
		.then(result => {
			return result;
		})
		.catch(err => {
			return {message: "Product ID is incorrect!"}
		})
};


/************************ Update Product Information *********************/
/*
	STEPS:
	1. Find product by Id using "findById"
	2. Update information
	3. Save the product
*/
module.exports.updateProduct = (reqParams, data, reqBody) => {
    
    let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	if (data.isAdmin) {
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
			if (error) {
				return false;
			}
			else {
				console.log(result);
				return {
					message : "Product Updated!",
					data: updatedProduct
				};
			}
		});
	}

};


/**************************** Archive a Product ************************/
/*
	STEPS:
	1. Find a product by Id and update using findByIdAndUpdate() method
	2. Set isActive to false
*/
module.exports.archiveProduct = (reqParams, data) => {
   
   let archiveProduct = {
   		isActive : false
   };
   if (data.isAdmin) {
	   return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((result, error) => {
	   		if (error) {
	   			return false;
	   		}
	   		else {
	   			console.log(result);
	   			return {
	   				message: "This product is now inactive!",
	   				data: archiveProduct
	   			};
	   		}
	   });
	}
	
};


/**************************** Activate a Product ************************/
/*
	STEPS:
	1. Find a product by Id and update using findByIdAndUpdate() method
	2. Set isActive to true
*/

module.exports.activateProduct = (reqParams, data) => {
   
   let activateProduct = {
   		isActive : true
   };
   if (data.isAdmin) {
	   return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then((result, error) => {
	   		if (error) {
	   			return false;
	   		}
	   		else {
	   			console.log(result);
	   			return {
	   				message: "This product is now active!",
	   				data: activateProduct
	   			};
	   		}
	   });
	}
	
};




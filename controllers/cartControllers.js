const Cart = require("../models/Cart");


/*****************************  Add-To-Cart ******************************/
module.exports.addToCart = async (data, reqBody) => {

	try {

		const newCart = new Cart({
			productId: reqBody.productId,
			price: reqBody.price,
			quantity: reqBody.quantity,
			total: reqBody.total * reqBody.quantity
		
		});


		return newCart.save().then((result, error) => {
					return {
						message: "New Cart Added!",
						data: result
					};
				});
		
	}
	catch (error) {
		return ({success: false, msg: error.message});
	}
};


/**************************  Retrieving All Cart ***********************/
module.exports.getAllCart = (data) => {

  return Cart.find({}).then(
    (result, error) => {
      if (error) {
        return false;
      }
        return result;
      }
    ); 
};


/***********************  Retrieving Specific Cart *********************/
module.exports.getSpecificCart = (data, reqParams) => {
	
	return Cart.findById(reqParams.id)
		.then(result => {
			return result;
		})
		.catch(err => {
			return {message: "Cart ID is incorrect!"}
		})
};




